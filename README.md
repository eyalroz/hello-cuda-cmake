# hello-cuda-cmake

This is a tiny, simplest-as-possible CUDA program, which is built using CMake to generate a build system. The CMake configuration file (`CMakeLists.txt`) is also minimal; with a more "meaty" CUDA application, you would likely need to use more commands and facilities (e.g. `find_package(CUDAToolkit)`) which a simple program is spared.

## Build instructions:

1. **Install CUDA**. You can get CUDA from [here](https://developer.nvidia.com/cuda-downloads); the choice compiler depends on which operating system you're using. Make sure the CUDA executables are on your `PATH` variable.
2. **Install CMake** version 3.18 or later is installed on your system (and again make sure its executable is on your `PATH`). You can get a binary CMake installation package from [here](https://cmake.org/download/).
3. **Get this repository's contents** by git-cloning downloading-and-unzipping into some folder.
4. (Make sure your system has a C++ compiler compatible with CUDA. If you're on Windows, you may need to be within a "Visual Studio Developr command shell/prompt".)
5. **Run `cmake -B build`**; that will set up a build environment for your project within a subdirectory named `build`.
6. **Run `cmake --build build**`; that will actually build the program by invoking the NVIDIA compiler (which will also use your host-side compiler).

That's it! You should now be able to run the program `build/hello`, and it should print `Hello CUDA world!`.

